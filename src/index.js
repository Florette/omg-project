import React from 'react';
import ReactDOM from 'react-dom';
import TelInput from './Tel-Input';
import './index.scss';

ReactDOM.render(
  <TelInput />,
  document.getElementById('root')
);
