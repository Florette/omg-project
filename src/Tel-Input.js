import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import SelectCountryCode from "./components/select";
import InputMask from "react-input-mask";
import countries from "./data.json"
import './tel-input.scss';

class TelInput extends Component {
    state = {
        open: false,
        selectedCountry: [
            "Russia",
            ["europe", "asia", "ex-ussr"],
            "ru",
            "7",
            "999-999-99-99",
            0
        ],
        number: ''
    };

    componentWillMount() {
        document.addEventListener('click', this.handleClickOutside, false);
    };

    componentWillUnmount() {
        document.removeEventListener('click', this.handleClickOutside, false);
    };

    handleClickOutside = (event) => {
        const domNode = ReactDOM.findDOMNode(this);
        if ((!domNode || !domNode.contains(event.target))) {
            this.setState({
                open : false
            });
        }
    };

    handleClick = () => {
        this.setState({
            open: !this.state.open
        })
    };

    handleItemClick = (country) => {
        this.setState({
            selectedCountry: country,
            open: false,
            number: ''
        })
    };

    handleInputChange = (event) => {
        this.setState({
            open: false,
            number: event.target.value
        });
    };

    render() {
        const {selectedCountry, open, number} = this.state;

        return (
            <div className="tel-input">
                <SelectCountryCode
                    open={open}
                    countries={countries}
                    handleClick={this.handleClick}
                    handleItemClick={this.handleItemClick}
                    selectedCountry={selectedCountry}
                />
                <InputMask mask={selectedCountry[4]}
                           placeholder={selectedCountry[4]}
                           onChange={e => this.handleInputChange(e)}
                           value={number}
                           maxLength={selectedCountry[4] ? '' : '15'}
                />
            </div>
        );
    }
}

export default TelInput;
