import React from 'react';
import IconArrow from "./svg-icon";

const SelectCountryCode = ({open, countries, selectedCountry, handleClick, handleItemClick}) => {
    return (
        <div className="country-code__wrapper">
            <div className="country-code__block" onClick={handleClick}>
                <div className={`country-code__list-img flag ${selectedCountry[2]}`}/>
                <div className="country-code__list-code">
                    +{selectedCountry[3]}
                </div>
                <IconArrow/>
            </div>
            {
                open && (
                    <div className={'country-code__list'}>
                        {
                            countries.map((country, index) => {
                                return (<div className="country-code__list-item" key={index}
                                             onClick={() => handleItemClick(country)}>
                                    <div className="country-code__list-wrap">
                                        <div className={`country-code__list-img flag ${country[2]}`}/>
                                        <div className="country-code__list-code">
                                            {country[3]}
                                        </div>
                                    </div>
                                    <div className="country-code__list-country">
                                        {country[0]}
                                    </div>
                                </div>)
                            })
                        }
                    </div>
                )
            }
        </div>
    );
};


export default SelectCountryCode;
