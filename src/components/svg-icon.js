import React from 'react';

const IconArrow = () => (
    <svg className="arrow" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
         viewBox="0 0 10 6">
		<title>Asset 17</title>
		<g id="Layer_2_1_">
			<g id="Content">
				<path d="M5,6C4.7,6,4.5,5.9,4.3,5.7l-4-4c-0.4-0.4-0.4-1,0-1.4s1-0.4,1.4,0L5,3.6l3.3-3.3c0.4-0.4,1-0.4,1.4,0s0.4,1,0,1.4l-4,4C5.5,5.9,5.3,6,5,6z"/>
			</g>
		</g>
    </svg>
);

export default IconArrow;
